//------------------------------------------------------------------------------
// Compile With:
// gcc -o dft dft.c -lsndfile
//------------------------------------------------------------------------------

#include <stdlib.h>
#include <math.h>
#include <sndfile.h>
#include <unistd.h>
#include <string.h>

#define TESTFILE "/sine.wav"
#define OUTPUT_HEADER "\nDFT ANALYSIS\n--------------------------------------------------------------------------------\n"
#define FLOAT_WIDTH 6

typedef struct {
  double re;
  double im;
} complex;

void dft(double* x, int N, complex X[]);
void idft(double* x, int N, complex X[]);
void readFreq(int N, int SR, double *f);
void readAmp(int N, double *amp, complex *X);
void readPhase(int N, double *phase, complex *X);

int main() {
  SF_INFO info;
  SNDFILE *input, *output;
  double *samples_1, *samples_2;

  char filepath[1024];
  getcwd(filepath, sizeof(filepath));
  strcat(filepath, TESTFILE);

  // Open Input File
  input = sf_open(filepath, SFM_READ, &info);

  // Allocate Buffer
  int len = info.frames;
  int sample_rate = info.samplerate;
  int channels = info.channels;
  samples_1 = malloc(sizeof(double) * len);
  samples_2 = malloc(sizeof(double) * len);


  // Read Input File to Buffer
  sf_read_double(input, samples_1, len);

  complex X[len];

  dft(samples_1, len, X);
  idft(samples_2, len, X);

  printf(OUTPUT_HEADER);
  printf("\nSamples: %i\nSample Rate: %i\nChannels: %i\n", len, sample_rate, channels);
  printf("\nDFT Output: \n");
  for(int n = 0; n < len; n++){
    printf("[%2d] x1 = %6.6f, X = (%6.6f, %6.6f), x2 = %6.6f\n", n, samples_1[n], X[n].re, X[n].im, samples_2[n]);
  }

  // Close Input & Output
  sf_close(input);

  // Deallocate Buffer
  free(samples_1);
  free(samples_2);

  return 0;
}

void dft(double* x, int N, complex X[]) {
  double pi2oN = 8.*atan(1.) / N;
  int k, n;
  for (k = 0; k < N; k++){
    X[k].re = X[k].im = 0.0;
    for (n = 0; n < N; n++){
      X[k].re += x[n] * cos(pi2oN * k * n);
      X[k].im -= x[n] * sin(pi2oN * k * n);
    }
    X[k].re /= N;
    X[k].im /= N;
  }
}

void idft(double* x, int N, complex X[]){
  double pi2oN = 8. * atan(1.) / N;
  double imag;
  int k, n;

  for (n = 0; n < N; n++){
    imag = x[n] = 0.0;
    for (k = 0; k < N; k++){
      x[n] += X[k].re * cos (pi2oN * k * n) - X[k].im * sin(pi2oN * k * n);
      imag += X[k].re * sin (pi2oN * k * n) + X[k].im * cos(pi2oN * k * n);
    }
    if (fabs (imag) > 1.e-5) fprintf( stderr, "warning: nonzero imaginary (%f) in waveform\n", imag);
  }
}
