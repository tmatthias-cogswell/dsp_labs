//------------------------------------------------------------------------------
// Compile With:
// gcc -std=c99 -o Builds/main main.c Assign2_Func/Functions.c -lsndfile
//------------------------------------------------------------------------------

#include "Assign2_Func/Functions.h"
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {

    // Build Demo Filepath Strings
    char cwd[1024];

    // Resource Filepaths
    char fp_mono_resource[1024];
    char fp_stereo_resource[1024];
    char fp_mono_mix_resource_1[1024];
    char fp_mono_mix_resource_2[1024];

    // Output Filepaths
    char fp_out_to_aiff[1024];
    char fp_out_to_aiff_stereo[1024];
    char fp_out_mono_to_stereo[1024];
    char fp_out_stereo_to_mono[1024];
    char fp_out_reverse[1024];
    char fp_out_mono_mixed[1024];
    getcwd(cwd, sizeof(cwd));

    // Build Resource Strings
    strcpy(fp_mono_resource, cwd);
    strcpy(fp_stereo_resource, cwd);
    strcpy(fp_mono_mix_resource_1, cwd);
    strcpy(fp_mono_mix_resource_2, cwd);

    strcat(fp_mono_resource, "/Builds/Resources/Kick_Test_Mono.wav");
    strcat(fp_stereo_resource, "/Builds/Resources/Guitar_Test_Stereo.wav");
    strcat(fp_mono_mix_resource_1, "/Builds/Resources/Mono_Mix_Signal_1.wav");
    strcat(fp_mono_mix_resource_2, "/Builds/Resources/Mono_Mix_Signal_2.wav");

    // Build Output Strings
    strcpy(fp_out_to_aiff, cwd);
    strcpy(fp_out_to_aiff_stereo, cwd);
    strcpy(fp_out_mono_to_stereo, cwd);
    strcpy(fp_out_stereo_to_mono, cwd);
    strcpy(fp_out_reverse, cwd);
    strcpy(fp_out_mono_mixed, cwd);

    strcat(fp_out_to_aiff, "/Builds/Outputs/fp_out_to_aiff.aiff");
    strcat(fp_out_to_aiff_stereo, "/Builds/Outputs/fp_out_to_aiff_stereo.aiff");
    strcat(fp_out_mono_to_stereo, "/Builds/Outputs/fp_out_mono_to_stereo.wav");
    strcat(fp_out_stereo_to_mono, "/Builds/Outputs/fp_out_stereo_to_mono.wav");
    strcat(fp_out_reverse, "/Builds/Outputs/out_reverse.wav");
    strcat(fp_out_mono_mixed, "/Builds/Outputs/mix.wav");

    // Run DSP Functions
    convert_file_to_AIFF(fp_mono_resource, fp_out_to_aiff);
    convert_file_to_AIFF(fp_stereo_resource, fp_out_to_aiff_stereo);
    convert_mono_to_stereo(fp_mono_resource, fp_out_mono_to_stereo);
    convert_stereo_to_mono(fp_stereo_resource, fp_out_stereo_to_mono);
    reverse_audio_file(fp_mono_resource, fp_out_reverse);
    mix_mono_signals(fp_mono_mix_resource_1, fp_mono_mix_resource_2, fp_out_mono_mixed);

}
