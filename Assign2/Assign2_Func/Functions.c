#include "Functions.h"
#include <stdlib.h>
#include <math.h>
#include <sndfile.h>

#define PI (acos(-1.0))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

void convert_file_to_AIFF(char* filepath, char* output_path){

  // Variable Declaration
  SNDFILE *infile, *outfile;
  SF_INFO sfInfo, sfOutInfo;
  double *copy_buffer;

  // Prep Info
  sfInfo.format = 0;
  sfOutInfo.format = 0;

  // Open Input File and Allocate
  infile = sf_open(filepath, SFM_READ, &sfInfo);
  copy_buffer = malloc(sizeof(double) * sfInfo.channels * sfInfo.frames);

  // Setup Output Information
  sfOutInfo.samplerate = sfInfo.samplerate;
  sfOutInfo.channels = sfInfo.channels;
  sfOutInfo.format = SF_FORMAT_AIFF | SF_FORMAT_PCM_16;

  // Open New File and reset frames (else sfOutInfo.frames = 0)
  outfile = sf_open(output_path, SFM_WRITE, &sfOutInfo);
  sfOutInfo.frames = sfInfo.frames;

  // Read to Buffer, copy buffer into output file
  sf_read_double(infile, copy_buffer, sfInfo.frames * sfInfo.channels);
  sf_write_double(outfile, copy_buffer, sfOutInfo.frames * sfOutInfo.channels);

  // Close Input & Output
  sf_close(infile);
  sf_close(outfile);

  // Deallocate Buffer
  free(copy_buffer);
}

void convert_mono_to_stereo(char* filepath, char* new_file_name){
    SNDFILE *infile, *outfile;
    SF_INFO sfinfo, sfOutInfo;
    sf_count_t readCount;
    double *input_buffer, *output_buffer;

    sfinfo.format=0;
    if (! (infile = sf_open(filepath, SFM_READ, &sfinfo))) {
        printf ("Not able to open input file %s.\n", filepath) ;
        sf_perror(NULL);
    }

    input_buffer = malloc(sfinfo.frames * sizeof(double));
    output_buffer = malloc(sfinfo.frames * 2 * sizeof(double));

    readCount = sf_read_double(infile, input_buffer, sfinfo.frames);

    for (int i = 0; i < sfinfo.frames * 2; i+=2) {
      output_buffer[i] = input_buffer[i/2];
    }

    for (int i = 1; i < sfinfo.frames * 2; i+=2) {
      if (i > 1) {
        output_buffer[i] = input_buffer[(int)(i/2)-1];
      }
    }

    // Potential Single For Loop
    // for (int i = 0; i < sfinfo.frames * 2; i++) {
    //     if (i % 2 == 0) {
    //         output_buffer[i] = input_buffer[i/2];
    //     }
    //
    //     else if (i % 2 != 0 && i > 1) {
    //         output_buffer[i] = input_buffer[(int) (i/2)-1];
    //     }
    //
    //     else if (i == 1){
    //         output_buffer[i] = input_buffer[i/2];
    //     }
    // }

    sfOutInfo.frames = sfinfo.frames * 2;
    sfOutInfo.samplerate = sfinfo.samplerate;
    sfOutInfo.channels = sfinfo.channels * 2;
    sfOutInfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

    if (! (outfile = sf_open(new_file_name, SFM_WRITE, &sfOutInfo))) {
        printf ("Not able to open input file %s.\n", filepath) ;
        sf_perror(NULL);
    }

    sf_write_double(outfile, output_buffer, sfinfo.frames*2);

    sf_close(infile);
    sf_close(outfile);
    free(input_buffer);
    free(output_buffer);
}

void convert_stereo_to_mono(char* filepath, char* new_file_name){
    SNDFILE *infile, *outfile;
    SF_INFO sfinfo, sfOutInfo;
    sf_count_t readCount;
    double *input_buffer, *output_buffer;

    sfinfo.format = 0;
    infile = sf_open(filepath, SFM_READ, &sfinfo);

    int len = sfinfo.frames;
    int channels = sfinfo.channels;
    input_buffer = malloc(sizeof(double) * len * channels);
    output_buffer = malloc(sizeof(double) * len);
    sf_read_double(infile, input_buffer, len * channels);

    for (int i = 0; i < sfinfo.frames; i++){
      output_buffer[i] = 0;
      for (int j = 0; j < sfinfo.channels; j++){
        output_buffer[i] += input_buffer[i * sfinfo.channels + j];
      }
      output_buffer[i] /= sfinfo.channels;
    }

    sfOutInfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
    sfOutInfo.channels = 1;
    outfile = sf_open(new_file_name, SFM_WRITE, &sfOutInfo);
    sf_write_double(outfile, output_buffer, len);

    sf_close(infile);
    sf_close(outfile);
    free(input_buffer);
    free(output_buffer);
}

int generateComplex(double *waveData, int sr, double freq, double *pfreq, double *amp, double *phase, int npshls, int length) {
    int writeCount=0;

    for(int n=0;n<length;n++) {
        for(int m=0;m<=npshls;m++) {
            waveData[n] += amp[m]*sin(freq * pfreq[m] * (2 * PI) * (n/sr)) + phase[m];
        }
        writeCount++;
    }
    return writeCount;
}

void normalize(double *waveData, int length) {
    double peak = 0.0;
    double normfac;
    for(int i = 0; i < length; i++) {
        if (waveData[i] > peak)
            peak = waveData[i];
    }

    normfac = 0.5 / peak;

    for(int i = 0; i < length; i++) {
        waveData[i] = waveData[i] * normfac;
    }
}

void reverse_audio_file(char* filepath, char* fileOutputPath){
    SF_INFO info;
    SNDFILE *input, *output;
    float *samples;
    int len, i;

    // Open Input File
    input = sf_open(filepath, SFM_READ, &info);

    // Allocate Buffer
    len = info.frames;
    samples = malloc(sizeof(float) * len);

    // Read Input File to Buffer
    sf_read_float(input, samples, len);

    // open output file
    output = sf_open(fileOutputPath, SFM_WRITE, &info);

    // Reverse through input buffer and write to output
    for (i = len-1; i >= 0; i--)
    {
        sf_write_float(output, &samples[i], 1);
    }

    // Close Input & Output
    sf_close(input);
    sf_close(output);

    // Deallocate Buffer
    free(samples);
}

void mix_mono_signals(char* input_fp_1, char* input_fp_2, char* output_fp){

  // Variable Declaration
  SNDFILE *file_input_1, *file_input_2, *file_output;
  SF_INFO sf_info_input_1, sf_info_input_2, sf_info_output;
  double *input_buffer_1, *input_buffer_2, *output_buffer;
  long long max_frame_length;
  int max_channels, min_sample_rate;

  // Open Input Files
  file_input_1 = sf_open(input_fp_1, SFM_READ, &sf_info_input_1);
  file_input_2 = sf_open(input_fp_2, SFM_READ, &sf_info_input_2);

  // Compare File Info
  max_frame_length = MAX(sf_info_input_1.frames, sf_info_input_2.frames);
  max_channels = MAX(sf_info_input_1.channels, sf_info_input_2.channels);
  min_sample_rate = MIN(sf_info_input_1.samplerate, sf_info_input_2.samplerate);

  // Allocate Buffers
  input_buffer_1 = malloc(sizeof(double) * sf_info_input_1.channels  * max_frame_length);
  input_buffer_2 = malloc(sizeof(double) * sf_info_input_2.channels  * max_frame_length);
  output_buffer = malloc(sizeof(double) * max_channels * max_frame_length);

  // Read Input File to Buffer
  sf_read_double(file_input_1, input_buffer_1, sf_info_input_1.frames);
  sf_read_double(file_input_2, input_buffer_2, sf_info_input_2.frames);

  // Setup Output Info
  sf_info_output.format = sf_info_input_1.format;
  sf_info_output.channels = max_channels;
  sf_info_output.samplerate = min_sample_rate;

  file_output = sf_open(output_fp, SFM_WRITE, &sf_info_output);
  sf_info_output.frames = max_frame_length;

  // Mix!
  for (int i = 0; i < max_frame_length; i++) {
    output_buffer[i] = ((input_buffer_1[i] + input_buffer_2[i]) - (input_buffer_1[i] * input_buffer_2[i]));
  }

  // Write To Output
  sf_write_double(file_output, output_buffer, max_frame_length * max_channels);

  // Close Files
  sf_close(file_input_1);
  sf_close(file_input_2);
  sf_close(file_output);

  // Deallocate
  free(input_buffer_1);
  free(input_buffer_2);
  free(output_buffer);
}
