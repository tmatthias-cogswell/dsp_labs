#ifndef DSP_LAB1_FUNCTIONS_H
#define DSP_LAB1_FUNCTIONS_H

void convert_file_to_AIFF(char* filepath, char* output_path);
void convert_mono_to_stereo(char* filepath, char* new_file_name);
void convert_stereo_to_mono(char* filepath, char* new_file_name);
int generateComplex(double *waveData, int sr, double freq, double *pfreq, double *amp, double *phase, int npshls, int length);
int normalize(double *waveData, int length);
void reverse_audio_file(char*, char*);
void mix_mono_signals(char* input_fp_1, char* input_fp_2, char* output_fp);

#endif
