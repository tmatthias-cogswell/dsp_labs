//
//  main.m
//  DSP-Lab1
//
//  Created by Thomas Matthias on 2/1/16.
//  Copyright © 2016 PurpleSun All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#define SAMPLE_RATE 44100
#define DURATION 5.0
#define FILENAME_FORMAT @"output.aif"
#define PI acos(-1)
#define twoPI 2 * acos(-1)

//Checks for command line input flags
bool input_check(int arg_num){
    if (arg_num < 3) {
        printf ("Usage: CAToneFileGenerator S \nWhere N is either sine/saw/square\n and H is Hz Value");
        return false;
    }
    else {
        return true;
    }
}

//Generates Filename
NSString* genFileName(NSString* FileNameFormat, double in_hz){
    NSString *filename = [NSString stringWithFormat: FileNameFormat, in_hz];
    return filename;
}

//Generates Filepath
NSString* genFilePath(NSString* input_fileName){
    NSString *filePath = [[[NSFileManager defaultManager]
                           currentDirectoryPath]stringByAppendingPathComponent: input_fileName];
    return filePath;
}

//Generates FileURL
NSURL* genFileURL(NSString* input_filePath){
    NSURL *fileURL= [NSURL fileURLWithPath: input_filePath];
    return fileURL;
}

AudioStreamBasicDescription genASBD(int inSampleRate){
    AudioStreamBasicDescription asbd;
    memset(&asbd, 0, sizeof(asbd));
    asbd.mSampleRate = inSampleRate;
    asbd.mFormatID = kAudioFormatLinearPCM;
    asbd.mFormatFlags = kAudioFormatFlagIsBigEndian | kAudioFormatFlagIsSignedInteger |kAudioFormatFlagIsPacked;
    asbd.mBitsPerChannel = 16;
    asbd.mChannelsPerFrame = 1;
    asbd.mFramesPerPacket = 1;
    asbd.mBytesPerFrame = 2;
    asbd.mBytesPerPacket = 2;
    return asbd;
}


int main (int argc, const char * argv[]) {
    @autoreleasepool {
        
        const char* test_str = argv[2];
        char* sine_test = "sine";
        char* saw_test = "saw";
        char* square_test = "square";
        
        if (input_check(argc) != true){
            return -1;
        }
        
        //Convert and confirm hz value
        double hz = atof(argv[1]);
        assert (hz > 0);
        
        // Set up the file
        AudioFileID audioFile;
        OSStatus audioErr = noErr;
        
        NSString* currentFileName = genFileName(FILENAME_FORMAT, hz);
        NSString* currentFilePath = genFilePath(currentFileName);
        NSURL* currentFileURL = genFileURL(currentFilePath);
        AudioStreamBasicDescription asbd = genASBD(SAMPLE_RATE);
        
        
        audioErr = AudioFileCreateWithURL((__bridge CFURLRef)currentFileURL, kAudioFileAIFFType, &asbd, kAudioFileFlags_EraseFile, &audioFile);
        assert (audioErr == noErr);
        
        long maxSampleCount = SAMPLE_RATE;
        long sampleCount = 0;
        UInt32 bytesToWrite = 2;
        double wavelengthInSamples = SAMPLE_RATE / hz;
        
        // Start writing samples
        
        // Sine Wave
        if (strcmp(test_str, sine_test) == 0){
            while (sampleCount < maxSampleCount){
                SInt16 sample = CFSwapInt16HostToBig((SInt16) SHRT_MAX * sin(twoPI * (sampleCount / wavelengthInSamples)));
                audioErr = AudioFileWriteBytes(audioFile, false, sampleCount*2, &bytesToWrite, &sample);
                assert (audioErr == noErr);
                sampleCount++;
            }
        }
        
        // Square Wave
        if (strcmp(test_str, square_test) == 0){
            while (sampleCount < maxSampleCount) {
                for (int i=0; i < wavelengthInSamples; i++) {
                    SInt16 sample;
                    if (i < wavelengthInSamples / 2) {
                        sample = CFSwapInt16HostToBig (SHRT_MAX);
                    }
                    else {
                        sample = CFSwapInt16HostToBig (SHRT_MIN);
                    }
                    audioErr = AudioFileWriteBytes(audioFile, false, sampleCount*2, &bytesToWrite, &sample);
                    assert (audioErr == noErr);
                    sampleCount++;
                }
            }
        }
        
        // Saw Wave
        if (strcmp(test_str, saw_test) == 0){
            while (sampleCount < maxSampleCount) {
                SInt16 sample = CFSwapInt16HostToBig (((sampleCount / wavelengthInSamples) * SHRT_MAX *2) -
                                                      SHRT_MAX);
                audioErr = AudioFileWriteBytes(audioFile,
                                               false,
                                               sampleCount*2,
                                               &bytesToWrite,
                                               &sample);
                assert (audioErr == noErr);
                sampleCount++;
            }
        }
        
        if (strcmp(argv[2], "square")) {
            
        }
        audioErr = AudioFileClose(audioFile);
        assert (audioErr == noErr);
        NSLog (@"wrote %ld samples", maxSampleCount);
    }
    return 0;
}

